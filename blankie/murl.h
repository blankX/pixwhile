#pragma once

#include <regex>
#include <string>

namespace blankie {
namespace murl {

struct Url {
    std::string scheme;
    std::string userinfo;
    std::string hostname;
    int port; // -1 if unspecified
    std::string path;
    std::string query;
    std::string fragment;

    Url(const std::string& str);

    inline std::string get_origin() const {
        std::string res;
        if (!this->scheme.empty()) {
            res = this->scheme + "://";
        }
        res += this->hostname;
        if (this->port != -1) {
            res += ':';
            res += std::to_string(this->port);
        }
        return res;
    }

    // NOT SECURE!
    bool is_host_equal(std::string other) const;

    std::string to_string() const;
};

std::string escape(const std::string& in);
std::string unescape(const std::string& in);
std::string normalize_path(const std::string& str);

} // namespace murl
} // namespace blankie
