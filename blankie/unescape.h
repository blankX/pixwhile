#pragma once

namespace blankie {
namespace html {

[[nodiscard]] std::string unescape(const std::string& str);

} // namespace html
} // namespace blankie
