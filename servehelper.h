#pragma once

#include <optional>

#include <httplib/httplib.h>
#include "blankie/murl.h"
#include "blankie/serializer.h"

struct Config; // forward declaration from config.h
struct Illusts; // forward declaration from pixivmodels.h
using Element = blankie::html::Element;
using Node = blankie::html::Node;
using Nodes = std::vector<Node>;

void serve(const httplib::Request& req, httplib::Response& res, const Config& config, std::string title, Element element, Nodes extra_head = {});
void serve_error(const httplib::Request& req, httplib::Response& res, const Config& config,
        std::string title, std::optional<std::string> subtitle = std::nullopt, std::optional<std::string> info = std::nullopt);
void serve_redirect(const httplib::Request& req, httplib::Response& res, const Config& config, std::string url, bool permanent = false);

std::string get_origin(const httplib::Request& req, const Config& config);
std::string proxy_url(blankie::murl::Url base, blankie::murl::Url url);
std::string proxy_image_url(const httplib::Request& req, const Config& config, blankie::murl::Url url);
bool should_send_304(const httplib::Request& req, uint64_t hash);

Element generate_illusts_pager(const httplib::Request& req, const Config& config, const Illusts& illusts, size_t page, const char* id);
