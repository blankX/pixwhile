Pixwhile is an alternative frontend to Pixiv that utilizes no Javascript.

## Features

- Does not contain an asinine amount of Javascript
- Allows you to open the original versions of cover images and profile pictures
- Can view illustrations and list user illustrations
- Can search for newest and oldest illustrations

## Missing Features

This list is not exhaustive, nor does it mean that these are being worked on.

- Can only search for newest and oldest illustrations
- No search
- No ability to login
- No ability to see comments or like counts

# Miscellaneous Information

In a page containing a list of illustrations (e.g. a user's illustrations page
or a search results page), illustrations containing multiple images will have a
badge indicating the amount of images inside. Illustrations that are marked as
being AI-generated will have a badge with a red background.
