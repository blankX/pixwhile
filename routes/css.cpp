#include <string>
#include <FastHash.h>

#include "routes.h"
#include "../servehelper.h"

static const constexpr char css[] = R"EOF(
    /* GENERAL */
    :root {
        --background-color: black;
        --text-color: white;

        --illust-badge-background-color: rgba(0, 0, 0, .5);
        --illust-badge-ai-background-color: rgba(255, 0, 0, .5);

        --error-background-color: rgb(100, 0, 0);
        --error-border-color: red;
        --error-text-color: white;

        --accent-color: #962AC3;
        --dark-accent-color: #7D3F7D;
        --bright-accent-color: #DE6DE6;
    }

    body {
        background-color: var(--background-color);
        color: var(--text-color);
        font-family: sans-serif;
    }
    img {
        max-width: 100%;
        height: auto; /* https://stackoverflow.com/a/17183996 */
        object-fit: cover;
    }
    .center {
        text-align: center;
        display: block;
    }

    a {
        color: var(--accent-color);
        text-decoration: none;
    }
    a:hover {
        color: var(--bright-accent-color);
        text-decoration: underline;
    }

    /* ILLUSTRATIONS GRID and ILLUSTRATION PREVIEW PAGE */
    .grid {
        gap: 1em;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    }
    .grid img {
        width: 15em;
        height: 15em;
    }

    /* ILLUSTRATIONS GRID (used in user illustrations page and search results page) */
    .illusts_grid p {
        width: 15em;
    }
    .illusts_grid-illust {
        position: relative;
    }
    .illusts_grid-illust_badge {
        position: absolute;
        top: .25em;
        right: .25em;
        padding: .25em;
        color: var(--text-color);
        background-color: var(--illust-badge-background-color);
        text-decoration: none !important;
    }
    .illusts_grid-illust_badge.ai {
        background-color: var(--illust-badge-ai-background-color);
    }

    /* USER PAGE AND ILLUSTRATIONS PAGE */
    .user_profile_picture {
        margin-right: .5em;
        width: 5em;
        height: 5em;
    }
    .user_metadata {
        display: flex;
        align-items: center;
        margin-left: .5em;
    }
    .user_profile_picture.small {
        width: 2.5em;
        height: 2.5em;
    }

    /* ILLUSTRATIONS PAGE */
    .illust-images {
        display: grid;
        text-align: center;
    }
    .illust-images .landmark {
        padding-top: 1em;
        padding-bottom: 1em;
    }
    .illust-tags {
        display: flex;
        flex-wrap: wrap;
        gap: 0 1em;
    }
    .illust-tags * {
        align-self: baseline;
    }

    /* USER PAGE */
    .user-cover {
        width: 100%;
        height: 50vh;
        margin-bottom: 1em;
    }

    /* SEARCH RESULTS PAGE */
    .search_results-suggestions {
        text-align: left;
        display: inline-block;
        margin-top: .5em;
        margin-bottom: 0;
    }

    /* ERROR PAGE */
    .error {
        text-align: center;
        background-color: var(--error-background-color);
        color: var(--error-text-color);
        border-style: solid;
        border-color: var(--error-border-color);
    }
)EOF";
#define CSS_LEN sizeof(css) / sizeof(css[0]) - 1

#if __cpp_constinit
    #define CONSTINIT constinit
#else
    #define CONSTINIT
#endif

CONSTINIT const uint64_t css_hash = FastHash(css, CSS_LEN, 0);

void css_route(const httplib::Request& req, httplib::Response& res) {
    res.set_header("ETag", std::string(1, '"') + std::to_string(css_hash) + '"');
    res.set_header("Cache-Control", "max-age=31536000, immutable");

    if (should_send_304(req, css_hash)) {
        res.status = 304;
        res.set_header("Content-Length", std::to_string(CSS_LEN));
        res.set_header("Content-Type", "text/css");
    } else {
        res.set_content(css, CSS_LEN, "text/css");
    }
}
