#pragma once

#include <httplib/httplib.h>
#include "../../blankie/serializer.h"
struct User; // forward declaration from ../../pixivmodels.h
struct Config; // forward declaration from ../../config.h

using Element = blankie::html::Element;

Element generate_user_header(const User& user, const httplib::Request& req, const Config& config);
