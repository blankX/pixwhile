#pragma once

#include <httplib/httplib.h>

struct Config; // forward declaration from ../config.h
class PixivClient; // forward declaration from ../pixivclient.h
class Redis; // forward declaration from ../hiredis_wrapper.h

extern const uint64_t css_hash;

void home_route(const httplib::Request& req, httplib::Response& res, const Config& config);
void css_route(const httplib::Request& req, httplib::Response& res);
void user_illustrations_route(const httplib::Request& req, httplib::Response& res, const Config& config, PixivClient& pixiv_client);
void artworks_route(const httplib::Request& req, httplib::Response& res, const Config& config, PixivClient& pixiv_client);
void tags_route(const httplib::Request& req, httplib::Response& res, const Config& config, PixivClient& pixiv_client);
void guess_extension_route(const httplib::Request& req, httplib::Response& res, const Config& config, PixivClient& pixiv_client, Redis* redis);
