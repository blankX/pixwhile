#pragma once

#include <string>

std::string quote(const std::string& str);
void throw_system_error(const char* what);
void throw_system_error(std::string what);
