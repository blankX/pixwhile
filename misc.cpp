#include <iomanip>
#include <sstream>
#include <system_error>

#include "misc.h"

std::string quote(const std::string& str) {
    std::stringstream ss;
    ss << std::quoted(str);
    return ss.str();
}

void throw_system_error(int err, const char* what) {
    if (err == ENOMEM) {
        throw std::bad_alloc();
    }
    throw std::system_error(err, std::generic_category(), what);
}

void throw_system_error(int err, std::string what) {
    if (err == ENOMEM) {
        throw std::bad_alloc();
    }
    throw std::system_error(err, std::generic_category(), std::move(what));
}

void throw_system_error(const char* what) {
    throw_system_error(errno, what);
}

void throw_system_error(std::string what) {
    throw_system_error(errno, std::move(what));
}
