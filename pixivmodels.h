#pragma once

#include <string>
#include <vector>
#include <optional>
#include <utility>

#include <nlohmann/json.hpp>

struct Image {
    std::string url;
    std::optional<std::pair<uint64_t, uint64_t>> size;

    Image(std::string url_) : url(std::move(url_)) {}
    Image(std::string url_, std::optional<std::pair<uint64_t, uint64_t>> size_) : url(std::move(url_)), size(std::move(size_)) {}
};

struct Images {
    std::optional<Image> original;
    std::vector<Image> thumbnails;

    const Image& original_or_thumbnail() const;
    const Image& thumbnail_or_original(size_t back = 0) const;
};

struct User {
    std::string username;
    std::string display_name;
    uint64_t user_id;

    std::optional<Images> cover_images;
    Images profile_pictures;
    std::vector<std::pair<std::string, std::string>> links;
    std::optional<std::string> ogp_image;
};

struct Tag {
    std::string japanese;
    std::optional<std::string> english;
};

struct Illust {
    std::string username;
    std::string user_display_name;
    uint64_t user_id;
    Images user_profile_pictures;

    uint64_t illust_id;
    std::string title;
    bool ai_generated;
    time_t upload_time;

    std::optional<std::string> comment_html;
    std::vector<Tag> tags;
    std::vector<Images> images;
    size_t page_count;
};

struct Illusts {
    std::vector<Illust> illusts;
    size_t total_illusts;
    size_t total_pages;
};

struct SearchResults {
    Illusts illusts;
    std::unordered_map<std::string, std::string> tag_translations;
};

struct SearchSuggestion {
    std::string tag;
    std::optional<std::string> english_tag;
};

void from_json(const nlohmann::json& j, User& user);
void from_json(const nlohmann::json& j, Tag& tag);
void from_json(const nlohmann::json& j, Illust& illust);
void from_json(const nlohmann::json& j, Illusts& illusts);
void from_json(const nlohmann::json& j, SearchResults& search_results);
void from_json(const nlohmann::json& j, SearchSuggestion& search_suggestion);
